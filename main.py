from my_trip_bot_handlers import bot, command_list, new_guest, search_city
from dictionary import *
from Support_func import logging
import time


@bot.message_handler(content_types=['text'])
def start_message(message) -> None:
    """
    Обрабатывает начальные сообщения от пользователя.
    :param message: Входящее сообщение от пользователя
    """
    new_guest.guest_id = message.from_user.id
    start_message.cid = message.chat.id
    if message.text in command_list:
        command_list[message.text](message)
    elif message.text in ['/lowprice', '/highprice', '/bestdeal']:
        new_guest.order_sort = message.text
        bot.send_message(message.from_user.id, info_message['ask_for_city']['ru_RU'], parse_mode='HTML')
        bot.register_next_step_handler(message, search_city)
    elif message.text.lower() == 'привет':
        bot.send_message(message.from_user.id, f'Привет {message.from_user.first_name}')
    else:
        bot.send_message(message.from_user.id, 'Я вас не поняла. Нажмите /help, для получения списка доступных команд')


@logging
def main():
    """Основная функция. Запускает работу телеграмм-бота"""
    try:
        bot.polling(non_stop=True, interval=0)
    except:
        bot.stop_polling()
        time.sleep(10)
        bot.polling(non_stop=True, interval=0)


if __name__ == '__main__':
    main()

import telebot
from bestdeal import bestdeal
from highprice import highprice
from lowprice import lowprice
from telebot import types
from typing import Dict, List
from dictionary import *
from history import history, read_history, clear_history
from Support_func import logging, set_lang, get_landmarks, get_address, photos_search, total_price
from telegram_bot_calendar import DetailedTelegramCalendar, LSTEP
from datetime import date
from decouple import config
import re
import requests
import json

bot = telebot.TeleBot(config('my_key'))


class Guest:
    def __init__(self) -> None:
        self.guest_id = None
        self.order_sort = None
        self.city_name = None
        self.city_id = None
        self.hotels_quantity = None
        self.photos_needed = True
        self.price_range = None
        self.distance = None
        self.lang = 'ru_RU'
        self.check_in = None
        self.check_out = None
        self.city_list = {}


new_guest = Guest()

command_list: Dict = {
    '/hello_world': lambda message: bot.send_message(message.from_user.id, 'Привет мир!'),
    '/help': lambda message: bot.send_message(message.from_user.id, '\n'.join(bot_command)),
    '/start': lambda message: bot.send_message(
        message.from_user.id, f"Привет {message.from_user.first_name}. "
                              f"Я бот {bot.user.first_name}, помогу тебе подобрать отель.")
}


@logging
def location_search(message: types.Message) -> Dict[str, str]:
    """
    Выполнение HTTP-запроса к Hotels API (rapidapi.com) (Поиск локаций (городов)).
    :param message: сообщение пользователя
    :return: словарь, содержащий сведения локаций (городов)
    """
    new_guest.city_name = message.text.strip()
    querystring = {"query": message.text.strip(), f"locale": f"{set_lang(message.text)}"}
    new_guest.lang = set_lang(message.text)
    response = requests.request("GET", city_url, headers=headers, params=querystring, timeout=10)
    if not response:
        bot.send_message(chat_id=message.chat.id, text=info_message['no_response'][set_lang(message.text)])
    else:
        data = json.loads(response.text)
        city_dict = {', '.join((city['name'],
                                re.findall('(\\w+)[\n<]', city['caption']+'\n')[-1])): city['destinationId']
                     for city in data['suggestions'][0]['entities']}
        return city_dict


@logging
def search_city(message: types.Message) -> None:
    """Обработка запроса пользователя по поиску города,
    вывод Inline клавиатуры с результатами"""
    temp = bot.send_message(chat_id=message.chat.id,
                            text=info_message['search'][set_lang(message.text)], parse_mode='HTML')
    city_list = location_search(message)
    keyboard = types.InlineKeyboardMarkup()
    if not city_list:
        bot.edit_message_text(chat_id=message.chat.id, message_id=temp.id,
                              text=info_message['no_options'][set_lang(message.text)], parse_mode='HTML')
    else:
        for city_name, city_id in city_list.items():
            keyboard.add(types.InlineKeyboardButton(text=city_name, callback_data=city_id))
        bot.edit_message_text(chat_id=message.chat.id, message_id=temp.id, text=info_message['city_results'][
            set_lang(message.text)], reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: any(key in call.message.text
                                                  for key in info_message['city_results'].values()))
@logging
def city_handler(call: types.CallbackQuery) -> None:
    """Обработка данных искомого города (id, name), определение следующего шага обработчика"""
    new_guest.city_id = call.data
    bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.id)
    if new_guest.order_sort == '/bestdeal':
        price_range_request(call.message)
    else:
        hotels_value_request(call.message)


@logging
def price_range_request(message: types.Message) -> None:
    """Запрос ценового диапазона у пользователя, определение следующего шага обработчика"""
    bot.send_message(chat_id=message.chat.id, text=info_message['ask_price'][new_guest.lang])
    bot.register_next_step_handler(message, dist_range_request)


@logging
def dist_range_request(message: types.Message) -> None:
    """Обработка значений ценового диапазона пользователя, запрос диапазона дистанции,
    определение следующего шага обработчика"""
    try:
        price_range = list(set(map(int, map(lambda string: string.replace(',', '.'),
                                            re.findall(r'\d+[.,\d+]?\d?', message.text)))))
        if new_guest.order_sort == '/bestdeal' and len(price_range) != 2:
            raise ValueError('Range Error')
        else:
            new_guest.price_range = price_range
            bot.send_message(chat_id=message.chat.id, text=info_message['ask_dist'][new_guest.lang])
            bot.register_next_step_handler(message, hotels_value_request)
    except ValueError:
        bot.send_message(chat_id=message.chat.id,
                         text=info_message['rng_err'][new_guest.lang])
        price_range_request(message)


@logging
def hotels_value_request(message: types.Message) -> None:
    """Обработка значений диапазона дистанции пользователя, запрос количества отелей,
    определение следующего шага обработчика"""
    try:
        distance_range = list(set(map(float, map(lambda string: string.replace(',', '.'),
                              re.findall(r'\d+[.,\d+]?\d?', message.text)))))
        if new_guest.order_sort == '/bestdeal' and len(distance_range) != 2:
            raise ValueError('Range Error')
        else:
            new_guest.distance = distance_range
            bot.send_message(chat_id=message.chat.id,
                             text=info_message['hotels_value'][new_guest.lang], parse_mode='HTML')
            bot.register_next_step_handler(message, photo_needed)
    except ValueError:
        bot.send_message(chat_id=message.chat.id,
                         text=info_message['rng_err'][new_guest.lang])
        dist_range_request(message)


@logging
def photo_needed(message: types.Message) -> None:
    """Обработка значения кол-ва отелей пользователя, запрос необходимости вывода фото в виде Inline клавитуары"""
    try:
        new_guest.hotels_quantity = abs(int(message.text))
        if new_guest.hotels_quantity > 10:
            raise ValueError('Value Error')
        else:
            keyboard = types.InlineKeyboardMarkup()
            [keyboard.add(types.InlineKeyboardButton(x, callback_data=x)) for x in
             [info_message['pos'][new_guest.lang], info_message['neg'][new_guest.lang]]]
            bot.send_message(message.chat.id, text=info_message['photo_needed'][new_guest.lang],
                             reply_markup=keyboard)
    except ValueError:
        bot.send_message(chat_id=message.chat.id,
                         text=info_message['val_err'][new_guest.lang])
        hotels_value_request(message)


@bot.callback_query_handler(
    func=lambda call: info_message['photo_needed'][new_guest.lang] in call.message.text)
@logging
def set_photo_needed(call: types.CallbackQuery) -> None:
    """Обработка ответа пользователя о необходимости вывода фото, определение хода действий."""
    bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.id)
    if any(call.data in answer for answer in info_message['pos'].values()):
        photo_quantity(call.message)
    else:
        new_guest.photos_needed = False
        check_in(call.message)


@logging
def photo_quantity(message: types.Message) -> None:
    """Запрос кол-ва фото у пользователя, определение следующего шага обработчика"""
    bot.send_message(message.chat.id,
                     info_message['photos_value'][new_guest.lang])
    bot.register_next_step_handler(message, value_of_photo_check)


@logging
def value_of_photo_check(message: types.Message) -> None:
    """Функция обрабатывает ответ пользователя по количеству фото, определяет следующий шаг обработчика"""
    if new_guest.photos_needed:
        new_guest.photos_needed = message.text
    if int(new_guest.photos_needed) <= 10:
        check_in(message)
    else:
        bot.send_message(message.chat.id, info_message['val_err'][new_guest.lang])
        photo_quantity(message)


@logging
def check_in(message: types.Message) -> None:
    """Обработка ответа пользователя о количестве фото, запрос даты заезда"""
    calendar, step = DetailedTelegramCalendar(calendar_id=1, locale=new_guest.lang[0:2]).build()
    bot.send_message(message.chat.id,
                     f"{info_message['check_in'][new_guest.lang]} {LSTEP[step]}",
                     reply_markup=calendar)


@bot.callback_query_handler(func=DetailedTelegramCalendar.func(calendar_id=1))
@logging
def cal(call: types.CallbackQuery):
    """Обработка ответа пользователя о дате заезда, определение следующего шага обработчика"""
    result, key, step = DetailedTelegramCalendar(calendar_id=1, locale=new_guest.lang[0:2]).process(call.data)
    cur_date = date.today()
    if not result and key:
        bot.edit_message_text(f"{info_message['no_date'][new_guest.lang]} {LSTEP[step]}",
                              call.message.chat.id,
                              call.message.message_id,
                              reply_markup=key)
    elif result:
        if cur_date > result:
            bot.send_message(call.message.chat.id, info_message['date_err'][new_guest.lang])
            check_in(call.message)
        else:
            bot.edit_message_text(f"{info_message['selected_date'][new_guest.lang]} {result}",
                                  call.message.chat.id,
                                  call.message.message_id)
            new_guest.check_in = result
            check_out(call.message)


@logging
def check_out(message: types.Message) -> None:
    """Запрос даты выезда"""
    if new_guest.check_in >= date.today():
        calendar, step = DetailedTelegramCalendar(calendar_id=2, locale=new_guest.lang[0:2]).build()
        bot.send_message(message.chat.id,
                         f"{info_message['check_out'][new_guest.lang]} {LSTEP[step]}",
                         reply_markup=calendar)
    else:
        bot.send_message(message.chat.id, info_message['date_err'][new_guest.lang])
        check_in(message)


@bot.callback_query_handler(func=DetailedTelegramCalendar.func(calendar_id=2))
@logging
def cal(call: types.CallbackQuery):
    """Обработка ответа пользователя о дате выезда, определение следующего шага обработчика"""
    result, key, step = DetailedTelegramCalendar(calendar_id=2, locale=new_guest.lang[0:2]).process(call.data)
    if not result and key:
        bot.edit_message_text(f"{info_message['no_date'][new_guest.lang]} {LSTEP[step]}",
                              call.message.chat.id,
                              call.message.message_id,
                              reply_markup=key)
    elif result:
        if new_guest.check_in > result:
            bot.send_message(call.message.chat.id, info_message['date_err'][new_guest.lang])
            check_out(call.message)
        else:
            bot.edit_message_text(f"{info_message['selected_date'][new_guest.lang]} {result}",
                                  call.message.chat.id,
                                  call.message.message_id)
            new_guest.check_out = result
            final(call.message)


@logging
def get_photos(hotel_id: int, text: str) -> List[types.InputMediaPhoto]:
    """
    Выполнение HTTP-запроса, обработка данных, содержащих фото вариантов размещения (отелей).
    :param hotel_id: hotel id
    :param text: информация об отеле
    :return: список фото варианта размещения (отеля)
    """
    photos = photos_search(hotel_id, int(new_guest.photos_needed))
    result = list()
    for i_photo in photos:
        if not result:
            result.append(types.InputMediaPhoto(caption=text, media=i_photo['baseUrl'].replace('{size}', 'w'),
                                                parse_mode='HTML'))
        else:
            result.append(types.InputMediaPhoto(media=i_photo['baseUrl'].replace('{size}', 'w')))
    return result


@bot.message_handler(commands=['history'])
@logging
def history_needed(message: types.Message) -> None:
    """Обработчик команд для операций с историей"""
    new_guest.guest_id = message.from_user.id
    keyword = types.InlineKeyboardMarkup(row_width=2)
    buttons = [types.InlineKeyboardButton(text=text, callback_data=text)
               for text in info_message['history_operations'][new_guest.lang]]
    keyword.add(*buttons)
    bot.send_message(chat_id=message.chat.id,
                     text=info_message['Available_history_operations'][new_guest.lang], reply_markup=keyword)


@bot.callback_query_handler(
    func=lambda call: info_message['Available_history_operations'][new_guest.lang] in call.message.text)
@logging
def show_history(call: types.CallbackQuery) -> None:
    """Обработка ответа пользователя по операциям с историей"""
    bot.delete_message(chat_id=call.message.chat.id, message_id=call.message.id)
    if any(call.data in answer for answer in info_message['history_operations'][new_guest.lang][:1]):
        try:
            data = read_history(str(new_guest.guest_id), str(new_guest.lang))
            for i_data in data:
                text = i_data
                bot.send_message(chat_id=call.message.chat.id, text=text)
        except TypeError:
            bot.send_message(chat_id=call.message.chat.id, text=info_message['no_history'][new_guest.lang])
    else:
        clear_history(str(new_guest.guest_id), str(new_guest.lang))
        bot.send_message(chat_id=call.message.chat.id, text=info_message['clr_history'][new_guest.lang])


@logging
def final(message: types.Message) -> None:
    """Обработка значения кол-ва фото, выполнение поиска вариантов, представление результатов"""
    temp = bot.send_message(chat_id=message.chat.id, text=info_message['search'][new_guest.lang])
    if new_guest.order_sort == '/bestdeal':
        hotels_dict, search_link = bestdeal(
            user_city_id=new_guest.city_id, lang=new_guest.lang, cur='RUB', hotels_value=new_guest.hotels_quantity,
            hotel_url=hotel_url, headers=headers, check_in=new_guest.check_in, check_out=new_guest.check_out,
            price_range=new_guest.price_range, dist_range=new_guest.distance
        )
        new_guest.city_list = hotels_dict
    elif new_guest.order_sort == '/highprice':
        hotels_dict, search_link = highprice(
            user_city_id=new_guest.city_id, lang=new_guest.lang, cur='RUB', hotels_value=new_guest.hotels_quantity,
            hotel_url=hotel_url, headers=headers, check_in=new_guest.check_in, check_out=new_guest.check_out,
        )
        new_guest.city_list = hotels_dict
    elif new_guest.order_sort == '/lowprice':
        hotels_dict, search_link = lowprice(
            user_city_id=new_guest.city_id, lang=new_guest.lang, cur='RUB', hotels_value=new_guest.hotels_quantity,
            hotel_url=hotel_url, headers=headers, check_in=new_guest.check_in, check_out=new_guest.check_out,
        )
        new_guest.city_list = hotels_dict
    if hotels_dict:
        history(guest_id=str(new_guest.guest_id), order_sort=str(new_guest.order_sort),
                city_list=new_guest.city_list, city_name=str(new_guest.city_name))
        bot.edit_message_text(chat_id=message.chat.id, message_id=temp.id,
                              text=info_message['ready_to_result'][new_guest.lang])
        for index, i_data in enumerate(hotels_dict.values()):
            if index + 1 > new_guest.hotels_quantity:
                break
            text = info_message['main_results'][new_guest.lang].format(
                name=i_data['name'], address=get_address(i_data), distance=get_landmarks(i_data),
                price=i_data['price'], total_price=total_price(str(new_guest.check_in), str(new_guest.check_out),
                                                               i_data['price']),
                link='https://hotels.com/ho' + str(i_data['id']),
                address_link='https://www.google.ru/maps/place/' + i_data['coordinate'])

            if new_guest.photos_needed:
                photo_list = get_photos(hotel_id=int(i_data['id']), text=text)
                for i_size in ['z', 'y', 'd', 'n', '_']:
                    try:
                        bot.send_media_group(chat_id=message.chat.id, media=photo_list)
                        break
                    except telebot.apihelper.ApiTelegramException:
                        photo_list = [types.InputMediaPhoto(caption=obj.caption, media=obj.media[:-5] + f'{i_size}.jpg',
                                                            parse_mode=obj.parse_mode) for obj in photo_list]
            else:
                bot.send_message(message.chat.id, text, parse_mode='HTML', disable_web_page_preview=True)

        bot.send_message(chat_id=message.chat.id, text=info_message['additionally'][new_guest.lang].format(
            link=search_link), parse_mode='MarkdownV2', disable_web_page_preview=True)
    else:
        bot.edit_message_text(chat_id=message.chat.id, message_id=temp.id,
                              text=info_message['no_options'][new_guest.lang])

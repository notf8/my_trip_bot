import functools
from datetime import datetime, timedelta
import re
from typing import Callable, Union, List, Dict, Any
import requests
from dictionary import *
import json


def logging(func: Callable) -> Callable:
    """Функция логер.
    Выводит на экран имена запускаемых функций и их документацию.
    Записывает в файл ошибки, возникающие при сбое в работе функций.
    :rtype: Callable
    """

    @functools.wraps(func)
    def wraps(*args, **kwargs):
        print('Вызывается функция {func}\t'
              'Документация функции: {docs}\t'.format(func=func.__name__, docs=func.__doc__))
        try:
            result = func(*args, **kwargs)
            return result
        except Exception as error:
            error = f'\nДата запуска: {datetime.now().strftime("%d.%m.%Y %H:%M:%S")} - ' \
                    f'Запускаемая функция: {func.__name__} - Тип ошибки:{error}'
            with open('function_errors.log', 'a', encoding='utf-8') as file:
                file.write(error)

    return wraps


def set_lang(text: str) -> str:
    """
    Функция определяет язык пользователя.
    :rtype: str
    """
    if bool(re.search(r'[А-Яа-я]', text)):
        return 'ru_RU'
    return 'en_US'


def photos_search(hotel_id: int, photo_quantity: int) -> List[Dict[str, Union[str, Any]]]:
    """
    Выполнение HTTP-запроса к Hotels API (rapidapi.com) (поиск фото).
    :param hotel_id: hotel id
    :param photo_quantity: Количество фото
    :return: список url-адресов фото варианта размещения (отеля)
    """

    querystring = {"id": "{}".format(hotel_id)}
    response = requests.request("GET", photo_url, headers=headers, params=querystring, timeout=10)
    photo_data = json.loads(response.text)
    photos_address = photo_data["hotelImages"][:photo_quantity]
    return photos_address


def get_address(city_dict: Dict[str, Any]) -> str:
    """
    Получаем адреса отеля
    :param city_dict: данные по отелям.
    :return: адрес отеля
    """
    return ', '.join(list(filter(lambda x: isinstance(x, str) and len(x) > 2, list(city_dict['address'].values()))))


def get_landmarks(city_dict: Dict[str, Any]) -> str:
    """
    Получаем ориентиры (объекты, расположенные неподалеку от отеля).
    :param city_dict: данные по отелям.
    :return: ориентиры расположения отеля
    """
    return ', '.join(['\n*{label}: {distance}'.format(label=info['label'], distance=info['distance'])
                      for info in city_dict['landmarks']])


def total_price(check_in: str, check_out: str, price: str) -> str:
    """
    Рассчитываем общую стоимость проживания.
    :param check_in: Дата заселения (str)
    :param check_out: Дата выезда (str)
    :param price: Стоимость проживания за одни сутки (str)
    :return: общая стоимость проживания в отеле
    :rtype: (str)
    """
    total_days = timedelta(int(check_out.replace('-', '')) - int(check_in.replace('-', '')))
    int_price = int(''.join(re.findall(r'\d', price)))
    if total_days.days != 0:
        result = int(total_days.days) * int_price
    else:
        result = int_price
    return '{:,d} RUB'.format(result)

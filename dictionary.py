bot_command = [
    'Доступные команды:',
    '\n/help - вывод списка доступных команд',
    '\n/hello_world - вывод базового приветствия',
    '\n/lowprice — вывод самых дешёвых отелей в городе',
    '\n/highprice — вывод самых дорогих отелей в городе',
    '\n/bestdeal — вывод отелей, наиболее подходящих по цене и расположению от центра',
    '\n /history — вывод истории поиска отелей.'

]

headers = {
    "X-RapidAPI-Key": "ef25172946mshc8894b1e6bed77fp1e9c9fjsnea1ccc2279f4",
    "X-RapidAPI-Host": "hotels4.p.rapidapi.com"
}

city_url = 'https://hotels4.p.rapidapi.com/locations/v2/search'
hotel_url = 'https://hotels4.p.rapidapi.com/properties/list'
photo_url = 'https://hotels4.p.rapidapi.com/properties/get-hotel-photos'


info_message = {
    'search': {
        'ru_RU': 'Выполняю поиск...',
        'en_US': 'Searching...'
    },
    'ask_for_city': {
        'ru_RU': 'Какой город Вас интересует?',
        'en_US': 'Which city are you interested in?'
    },
    'hotels_value': {
        'ru_RU': 'Сколько отелей смотрим? (не более 10)',
        'en_US': 'How many hotels are we looking at? (no more than 10)'
    },
    'photo_needed': {
        'ru_RU': 'Интересуют фотографии отелей?',
        'en_US': 'Interested in photos of the hotels?'
    },
    'photos_value': {
        'ru_RU': 'Сколько фотографий по каждому отелю? (не более 10)',
        'en_US': 'How many photos for each hotel? (no more than 10)'
    },
    'city_results': {
        'ru_RU': 'Уточните район города для поиска:',
        'en_US': 'Specify the area of the city to search:'
    },
    'city_name': {
        'ru_RU': 'Город поиска',
        'en_US': 'Search City'
    },
    'pos': {
        'ru_RU': 'Да',
        'en_US': 'Yes'
    },
    'neg': {
        'ru_RU': 'Нет',
        'en_US': 'No'
    },
    'check_in': {
        'ru_RU': 'Выберите дату заселения',
        'en_US': 'Select check-in date'
    },
    'check_out': {
        'ru_RU': 'Выберите дату выезда',
        'en_US': 'Select check-out date'
    },
    'ready_to_result': {
        'ru_RU': 'Я нашла для тебя следующие варианты...',
        'en_US': 'I found the following options for you...'
    },
    'main_results': {
        'ru_RU': "\n\n{name}"
                 "\n\nПосмотреть на картах:\n<a href='{address_link}'>{address}</a>"
                 "\n\nОриентиры: {distance}"
                 "\n\nЦена за ночь: {price}"
                 "\n\nЦена за все время проживания: {total_price}"
                 "\n\n<a href='{link}'>Подробнее на hotels.com</a>",
        'en_US': "\n\n{name}"
                 "\n\nView on maps:<a href='{address_link}'>{address}</a>"
                 "\n\nLandmarks: {distance}"
                 "\n\nPrice per night: {price}"
                 "\n\nPrice per stay: {total_price}"
                 "\n\n<a href='{link}'>More на hotels.com</a>"
    },
    'history_results': {
            'ru_RU': "\n\nДата поиска: {date}"
                     "\n\nГород поиска: {search_city}"
                     "\n\n{results}",
            'en_US': "\n\nSearch date: {date}"
                     "\n\nSearch city: {search_city}"
                     "\n\n{results}"
        },
    'additionally': {
        'ru_RU': 'Ничего не приглянулось?\nПопробуйте поискать на сайте\\: [смотреть]({link})'
                 '\nХотите продолжить работу с ботом? /help',
        'en_US': "Didn't like anything??\nTry to search the site\\: [view]({link})"
                 "\nDo you want to continue working with the bot? /help"
    },
    'val_err': {
        'ru_RU': 'Необходимо ввести целое число (не более 10):',
        'en_US': 'You must enter an integer (no more than 10):'
    },
    'rng_err': {
        'ru_RU': 'Необходимо ввести два целых положительных отличных друг от друга числа:',
        'en_US': 'It is necessary to enter two positive integers that are different from each other:'
    },
    'ask_price': {
        'ru_RU': 'Уточните ценовой диапазон:'
                 '\n(Например: "от 1000 до 2000", "1000-2000", "1000 2000")',
        'en_US': 'Specify the price range:'
                 '\n(As example: "from 1000 to 2000", "1000-2000", "1000 2000")'
    },
    'ask_dist': {
        'ru_RU': 'Уточните диапазон расстояния, на котором находится отель от центра (км):'
                 '\n(Например: "от 1 до 3" / "1-3" / "1 3")',
        'en_US': 'Specify the range of the distance at which the hotel is located from the center (mile)'
                 '\n(As example: "from 1 to 3" / "1-3" / "1 3"'
    },
    'no_options': {
        'ru_RU': 'По вашему запросу ничего не найдено...\n/help',
        'en_US': 'Nothing was found for your query...\n/help'
    },
    'no_response': {
        'ru_RU': 'Сервер не отвечает, попробуйте повторить запрос...\n/help',
        'en_US': 'The server is not responding, please try again...\n/help'
    },
    'date_err': {
        'ru_RU': 'Нельзя выбрать дату более раннюю, чем сегодняшний день, либо день заселения.\n',
        'en_US': 'You can not select a date earlier than today, or the day of settlement.\n'
    },
    'selected_date': {
        'ru_RU': 'Выбрана дата:\n',
        'en_US': 'Date selected:\n'
    },
    'no_date': {
        'ru_RU': 'Выберите необходимую дату\n',
        'en_US': 'Choose the required date\n'
    },
    'Available_history_operations': {
        'ru_RU': 'Доступные операции с историей',
        'en_US': 'Available operations with history'
    },
    'history_operations': {
        'ru_RU': ('Показать историю', 'Очистить историю'),
        'en_US': ('Show history', 'Clear the history')
    },
    'no_history': {
        'ru_RU': 'Похоже, история ваших запросов пуста.',
        'en_US': 'Looks like your search history is empty.'
    },
    'clr_history': {
        'ru_RU': 'Ваша история поиска очищена!',
        'en_US': 'Your search history has been cleared!'
    },
    'date_request': {
        'ru_RU': 'Дата запроса',
        'en_US': 'Date of request'
    },
    'sorting_type': {
        'ru_RU': 'Тип сортировки',
        'en_US': 'sort type'
    }
}

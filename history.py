from datetime import datetime
from typing import Union
from dictionary import *
from Support_func import logging
import json


@logging
def history(guest_id: str, order_sort: str, city_list: dict, city_name: str) -> None:
    """
    Функция предназначена для записи итоговых результатов по запросу пользователя в файл 'data_history'
    """
    date = datetime.now().strftime("%d.%m.%Y")
    for i_value in city_list.values():
        name = i_value['name']
        link = 'https://hotels.com/ho' + str(i_value['id']) + '\n'
        header = ''.join(str(name) + '\n' + str(link) + order_sort)
        try:
            with open('data_history.json', 'r', encoding='utf-8') as file:
                history_dict = json.load(file)
                if guest_id not in history_dict.keys():
                    history_dict[guest_id] = {'date': {date: {city_name: [header]}}}
                elif date not in history_dict[guest_id]['date'].keys():
                    history_dict[guest_id]['date'].update({date: {city_name: [header]}})
                elif city_name not in history_dict[guest_id]['date'][date]:
                    history_dict[guest_id]['date'][date][city_name] = [header]

                else:
                    history_dict[guest_id]['date'][date][city_name].append(header)

                with open('data_history.json', 'w', encoding='utf-8') as new_file:
                    json.dump(history_dict, new_file, indent=4, ensure_ascii=False)
        except FileNotFoundError:
            history_dict = dict()
            history_dict[guest_id] = {'date': {date: {city_name: [header]}}}
            with open('data_history.json', 'w', encoding='utf-8') as file:
                json.dump(history_dict, file, indent=4, ensure_ascii=False)


@logging
def read_history(guest_id: str, lang: str) -> Union[list[str], None]:
    """
    Функция считывает запросы пользователя из файла 'data_history', и возвращает результат в виде списка.
    :return: Информация о запросе пользователя в виде: Дата запроса/Город поиска/Список отелей/Тип сортировки
    :rtype: list
    """
    try:
        with open('data_history.json', 'r', encoding='utf-8') as file:
            history_dict = json.load(file)
            data = history_dict.get(guest_id)
            if data is not None:
                my_list = []
                for i_data in data.values():
                    for i_key, i_values in i_data.items():
                        for j_key in i_values.items():
                            text = info_message['history_results'][lang].format(
                                date=i_key, search_city=j_key[0],
                                results='\n'.join(elem for elem in j_key[1])
                            )
                            my_list.append(text)

                return my_list
            return None
    except FileNotFoundError:
        return None


@logging
def clear_history(guest_id: str, lang: str) -> str:
    """
    Функция удаляет историю пользователя, возвращает текстовое сообщение по итогам своей работы.
    :return: Текстовое сообщение об итогах работы.
    :rtype: str
    """
    try:
        with open('data_history.json', 'r', encoding='utf-8') as file:
            history_dict = json.load(file)
            history_dict.pop(guest_id, info_message['no_history'][lang])
        with open('data_history.json', 'w', encoding='utf-8') as file:
            json.dump(history_dict, file, indent=4, ensure_ascii=False)
    except FileNotFoundError:
        return info_message['no_history'][lang]
